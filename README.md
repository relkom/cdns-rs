# CDNS-rs ![600 logo](https://gitlab.com/4neko/cdns-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)


v 0.2 unstable

An implementation of client side DNS query library which also is able to look for host name in `/etc/hosts`.  
Also it is able to `/etc/resolv.conf` and uses options from this file to configure itself. So it acts like libc's `gethostbyname(3)` or `gethostbyaddr(3)`. The configuration can be overriden.

This library supports both async and sync code. At the moment async part is not available because:
- it is based on the sync realization because async code is based on sync code and sync code is unstable at the moment
- it requires proper porting from sync, because sync uses `poll(2)` to achieve the parallel name resolution

## Supported

- Sending and receiving responses via TCP/UDP
- Reacting on the message truncated event by trying TCP
- Parsing /etc/hosts (all options)
- Partial parsing /etc/resolve.conf (all options)
- Async and Sync code (separate implementations) At the moment async is not available!
- Parallel and non parallel nameserver quries

## ToDo

- Parse /etc/nsswitch.conf
- DNSSEC
- DNS-over-TLS
- OPT_NO_CHECK_NAMES
- resolv.conf (search, domain, sortlist)


Usage:  

- see ./examples/
- see shortcuts.rs

Simple Example:

```Rust
use cdns_rs::sync::{QDns, QuerySetup, QType, request, caches::CACHE};

fn main()
{
    // a, aaaa
    let res_a = request::resolve_fqdn("protonmail.com", None).unwrap();

    println!("A/AAAA:");
    for a in res_a
    {
        println!("\t{}", a);
    }
}
```

Custom query:

```Rust
use cdns_rs::sync::{QDns, QuerySetup, QType, request, caches::CACHE};

fn main()
{
    // soa
    let mut dns_req = 
        QDns::make_empty(resolvers, 1, QuerySetup::default());

    dns_req.add_request(QType::SOA, "protonmail.com");

    // sending request and receiving results
    let res = dns_req.query();


    println!("SOA:");
    if res.is_results() == true
    {
        let inner = res.into_inner().unwrap();

        for i in inner
        {
            for r in i.get_responses()
            {
                println!("\t{}", r);
            }
        }
    }
    else
    {
        println!("\tNo SOA found!")
    }
}
```
